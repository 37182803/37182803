public class Examen {
    private int clave;
    private String nombre;
    private int no_aciertos;
    private boolean activo;

    public Examen(int clave, String nombre, int no_aciertos, boolean activo) {
        this.clave = clave;
        this.nombre = nombre;
        this.no_aciertos = no_aciertos;
        this.activo = activo;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNo_aciertos() {
        return no_aciertos;
    }

    public void setNo_aciertos(int no_aciertos) {
        this.no_aciertos = no_aciertos;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
