import java.util.List;

public class Grupo {
    private int clave;
    private String nombre;
    private String semestre;
    private String grado;

    public Grupo(int clave, String nombre, String semestre, String grado) {
        this.clave = clave;
        this.nombre = nombre;
        this.semestre = semestre;
        this.grado = grado;
    }
    private Alumno[] listaAlumnos;
    private Materia[] listaMaterias;
    public Alumno AddAlumno(Alumno alumno){
        return alumno;
    }
    public Materia AddMateria(Materia materia){
        return materia;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }
}
