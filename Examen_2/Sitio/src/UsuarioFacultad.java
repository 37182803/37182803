import java.util.List;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
public class UsuarioFacultad {
    public static void menuUsuarioFacultad() {
        Scanner scanner = new Scanner(System.in);
        int opcion;

        do {
            System.out.println("Menú Usuario de Facultad:");
            System.out.println("1. Reporte de Participantes");
            System.out.println("2. Cambiar fecha límite");
            System.out.println("0. Volver al menú principal");
            System.out.print("Seleccione una opción: ");
            opcion = scanner.nextInt();
            scanner.nextLine();

            switch (opcion) {
                case 1:
                    generarReporte();
                    break;
                case 2:
                    cambiarFechaLimite();
                    break;
                case 0:
                    System.out.println("Volviendo al menú principal...");
                    break;
                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");
            }
            System.out.println();
        } while (opcion != 0);

        scanner.close();
    }

    private static void generarReporte() {
        List<Participante> participantes = Participante.leerParticipantes();
        if (participantes.isEmpty()) {
            System.out.println("No hay participantes registrados.");
        } else {
            System.out.println("Generando reporte de participantes...");
            // Lógica para generar el reporte en CSV
            generarReporteCSV(participantes);
        }
    }

    private static void generarReporteCSV(List<Participante> participantes) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(ReunionEgresados.REPORT_CSV))) {
            writer.println("Nombre,Apellido,Correo,Generación,Tipo de Institución,Carrera");

            for (Participante participante : participantes) {
                writer.println(participante.toCsvString());
            }

            System.out.println("Reporte generado exitosamente.");
        } catch (IOException e) {
            System.out.println("Error al generar el reporte: " + e.getMessage());
        }
    }

    private static void cambiarFechaLimite() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la nueva fecha límite: ");
        String nuevaFecha = scanner.nextLine();
        // Lógica para cambiar la fecha límite

        System.out.println("Fecha límite actualizada exitosamente.");

        scanner.close();
    }
}
