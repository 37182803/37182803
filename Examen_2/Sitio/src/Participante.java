import java.io.*;
import java.util.*;

public class Participante {
    private String nombre;
    private String apellido;
    private String correo;
    private int generacion;
    private String tipoInstitucion;
    private String carrera;

    public Participante(String nombre, String apellido, String correo, int generacion, String tipoInstitucion, String carrera) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.generacion = generacion;
        this.tipoInstitucion = tipoInstitucion;
        this.carrera = carrera;
    }

    public static void registrarParticipante() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Registro de Participante");
        System.out.print("Ingrese el nombre: ");
        String nombre = scanner.nextLine();
        System.out.print("Ingrese el apellido: ");
        String apellido = scanner.nextLine();
        System.out.print("Ingrese el correo electrónico: ");
        String correo = scanner.nextLine();
        System.out.print("Ingrese la generación: ");
        int generacion = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Seleccione una carrera:");
        mostrarCarreras();
        System.out.print("Ingrese el número de la carrera: ");
        int carreraIndex = scanner.nextInt();
        scanner.nextLine();
        String carrera = obtenerCarreraPorIndex(carreraIndex);

        System.out.print("Ingrese el tipo de institución: ");
        String tipoInstitucion = scanner.nextLine();

        Participante participante = new Participante(nombre, apellido, correo, generacion, tipoInstitucion, carrera);
        participante.guardarParticipante();

        System.out.println("¡Participante registrado exitosamente!");

        scanner.close();
    }

    private static void mostrarCarreras() {
        String[] carreras = {
                "Ingeniería en Computación",
                "Ingeniería de Software",
                "Ingeniería Eléctrica",
                "Ingeniería en Electrónica Industrial",
                "Ingeniería en Diseño Industrial",
                "Ingeniería en Robótica y Mecatrónica"
        };

        for (int i = 0; i < carreras.length; i++) {
            System.out.println((i + 1) + ". " + carreras[i]);
        }
    }

    private static String obtenerCarreraPorIndex(int index) {
        String[] carreras = {
                "Ingeniería en Computación",
                "Ingeniería de Software",
                "Ingeniería Eléctrica",
                "Ingeniería en Electrónica Industrial",
                "Ingeniería en Diseño Industrial",
                "Ingeniería en Robótica y Mecatrónica"
        };

        if (index >= 1 && index <= carreras.length) {
            return carreras[index - 1];
        }

        return "";
    }

    private void guardarParticipante() {
        try (PrintWriter writer = new PrintWriter(new FileWriter(ReunionEgresados.PARTICIPANTES_CSV, true))) {
            writer.println(toCsvString());
        } catch (IOException e) {
            System.out.println("Error al guardar el participante: " + e.getMessage());
        }
    }

    public String toCsvString() {
        return nombre + "," + apellido + "," + correo + "," + generacion + "," + tipoInstitucion + "," + carrera;
    }

    public static List<Participante> leerParticipantes() {
        List<Participante> participantes = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(ReunionEgresados.PARTICIPANTES_CSV))) {
            while (scanner.hasNextLine()) {
                String[] datos = scanner.nextLine().split(",");
                if (datos.length == 6) {
                    String nombre = datos[0];
                    String apellido = datos[1];
                    String correo = datos[2];
                    int generacion = Integer.parseInt(datos[3]);
                    String tipoInstitucion = datos[4];
                    String carrera = datos[5];

                    Participante participante = new Participante(nombre, apellido, correo, generacion, tipoInstitucion, carrera);
                    participantes.add(participante);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("No se pudo encontrar el archivo de participantes.");
        }

        return participantes;
    }
}

