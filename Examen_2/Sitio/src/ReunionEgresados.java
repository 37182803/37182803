import java.util.Scanner;

public class ReunionEgresados {
    public static final String PARTICIPANTES_CSV = "participantes.csv";
    public static final String REPORT_CSV = "reporte.csv";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcion = -1; // Valor predeterminado para la opción

        while (opcion != 0) {
            System.out.println("Menú Principal:");
            System.out.println("1. Participante");
            System.out.println("2. Usuario de Facultad");
            System.out.println("0. Salir");
            System.out.print("Seleccione una opción: ");

            if (scanner.hasNextInt()) {
                opcion = scanner.nextInt();
                scanner.nextLine(); // Consumir el salto de línea después de leer el número

                switch (opcion) {
                    case 1:
                        Participante.registrarParticipante();
                        break;
                    case 2:
                        UsuarioFacultad.menuUsuarioFacultad();
                        break;
                    case 0:
                        System.out.println("Saliendo del programa...");
                        break;
                    default:
                        System.out.println("Opción inválida. Por favor, seleccione una opción válida.");
                }
                System.out.println();
            } else {
                System.out.println("Entrada inválida. Por favor, ingrese un número válido.");
                scanner.nextLine(); // Consumir la entrada inválida
            }
        }

        scanner.close();
    }
}
